# pj2me
Startup project for J2ME games.

- Screen manager interface to support quickly addition of scenes
- Font, FPS and Sound managers
- Helpers to support lcdui package, screen resolution and graphics

Thanks to collaborators:
Diogo Autílio /
Fábio Attard
